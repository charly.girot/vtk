#ifndef vtkopenvrtimercallback_h
#define vtkopenvrtimercallback_h



#include <vtkSmartPointer.h>
#include <vtkCommand.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include "vtkOpenVRTrackedCamera.h"

class vtkOpenVRTrackedCamera;
class vtkOpenVRTimerCallback : public vtkCommand
{
public:
	static vtkOpenVRTimerCallback *New()
	{
		vtkSmartPointer<vtkOpenVRTimerCallback> cb = vtkSmartPointer<vtkOpenVRTimerCallback>::New();
		cb->TimerCount = 0;
		//cb->tcam = nullptr;
		
		return cb;
	}

	virtual void Execute(vtkObject *vtkNotUsed(caller), unsigned long eventId, void *vtkNotUsed(callData))
	{
		if (vtkCommand::TimerEvent == eventId)
		{
			++this->TimerCount;
			std::cout << "Coucou " << TimerCount << std::endl; 
		}
		cout << this->TimerCount;
//		this->tcam->DisplayRefreshTimeout();

	}
protected:
	vtkOpenVRTimerCallback()
	{
		//this->TimerCount = 0;
	}
	~vtkOpenVRTimerCallback();

private:
	int TimerCount;
	//vtkOpenVRTrackedCamera *tcam;// = vtkOpenVRTrackedCamera::New();
};


#endif // vtkopenvrtimercallback_h