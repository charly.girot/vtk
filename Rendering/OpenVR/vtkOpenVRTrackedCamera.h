/*=========================================================================

Program:   Visualization Toolkit
Module:    vtkOpenVRTrackedCamera.h

--Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
--All rights reserved.
--See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
* @class   vtkOpenVRTrackedCamera
* @brief   OpenVR tracked camera
*
* vtkOpenVRFrontCamera support for VR Front camera (or trackedCamera)
*/

#ifndef vtkopenvrtrackedcamera_h
#define vtkopenvrtrackedcamera_h

#include "vtkRenderingOpenVRModule.h" // For export macro
#include "vtkObject.h"

#include "vtkEventData.h" // for enums
#include "vtkFloatArray.h" //for texture coordinates
#include "vtkOpenVRCamera.h" //To differentiate background camera when rendering video mode
#include "vtkPassThrough.h" //Connect vtk Image data to the texture
#include "vtkPointData.h" //texture coordinates
#include "vtkPolyData.h" //representation of the video mode
#include "vtkPolyDataMapper.h"
#include "vtkTexture.h"

#include <openvr.h> //for ivars

class vtkOpenVRTrackedCameraPreview;

class VTKRENDERINGOPENVR_EXPORT vtkOpenVRTrackedCamera : public vtkProp
{
public:
  static vtkOpenVRTrackedCamera *New();
  vtkTypeMacro(vtkOpenVRTrackedCamera, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override { this->Superclass::PrintSelf(os, indent); }

  /**
  * Initialize the tracked camera
  */
  virtual void Initialize(vtkOpenVRRenderWindow *rw);

  /**
  * Start Video Preview
  * Should only be used internaly
  */
  void StartVideoPreview();

  /**
  * Stop Video Preview
  * Should only be used internaly
  */
  void StopVideoPreview();

  /**
  * Update the position of the screen displaying what the front camera sees
  * Follow the user to be always in front (similar to what vtkFollower does)
  */
  void UpdateDisplayPosition();

  /**
  * Refresh the texture/screen displaying what sees the camera after Timeout
  */
  void DisplayRefreshTimeout();

  //@{
  /**
  * Set / get the state of the tracked camera.
  * This defines if the tracked camera is enabled.
  * SetEnabled is not defined with vtkSetMacro cause
  * it's calling to stopVideoPreview for memory use reason.
  */
  void SetEnabled(bool enabled);
  vtkGetMacro(Enabled, bool);
  vtkBooleanMacro(Enabled, bool);

  //@{
  /**
  * Set / get enable drawing of the video mode tracked camera.
  * This defines if the tracked camera is enabled to be
  * drawn with the video mode (square in front of the user).
  */
  void SetDrawingEnabled(bool enabled);
  vtkGetMacro(DrawingEnabled, bool);
  vtkBooleanMacro(DrawingEnabled, bool);

  /**
  * Build representation of the video mode for tracked camera
  */
  void BuildRepresentation();

  /**
  * Accesor to the preview image which contains a
  * vtkImage Data with the camera texture
  */
  vtkGetMacro(CameraPreviewImage, vtkOpenVRTrackedCameraPreview*);

  /**
  * Accesor to the VRTrackedCamera
  */
  vr::IVRTrackedCamera* GetVRTrackedCamera() { return this->VRTrackedCamera; }

  //@{
  /**
  * Set / get the Renderer associated with TrackedCamera.
  */
  virtual void SetRenderer(vtkRenderer *ren);
  virtual vtkRenderer* GetRenderer();
  //@{
  /**
  * Set / get the Frame Type to be used.
  * 0 - Distorted
  * 1 - Undistorted
  * 2 - Maximum_Undistorted
  * For more indications about the differences, check OpenVR API Documentation
  */
  void SetFrameType(int type);
  int GetFrameType();

  /**
  *Return frame type as a string for user understanding
  *The type must be furnished as int
  */
  std::string GetEnumFromFrameType(int type);
  /**
  * Return the current frame type as a string for user understanding
  */
  std::string GetEnumFromFrameType();

protected:
  vtkOpenVRTrackedCamera();;
  ~vtkOpenVRTrackedCamera();

  /** Vive System */
  vr::IVRSystem *pHMD;

  /** Tracked Camera (or front camera) */
  vr::IVRTrackedCamera *VRTrackedCamera;

  /** The tracked Camera has a unique TrackedCameraHandle_t
  * This handle is used to set attributes, receive events, (and render?).
  * These are several circumstances where the tracked camera isn't detected or invalid.
  * In those case the handle will be equal to INVALID_TRACKED_CAMERA_HANDLE */
  vr::TrackedCameraHandle_t VRTrackedCameraHandle;


  /** Camera frame parameters */
  uint32_t				CameraFrameWidth;
  uint32_t				CameraFrameHeight;
  uint32_t				CameraFrameBufferSize;
  uint8_t					*CameraFrameBuffer;

  uint32_t				LastFrameSequence;

  /** Pointer to the parent vtkOpenVRRenderWindow */
  vtkSmartPointer<vtkOpenVRRenderWindow> RenWin;

  /** Container for the preview image to render */
  vtkOpenVRTrackedCameraPreview *CameraPreviewImage;

  /** Render event - used to update position of the TrackedCameraActor */
  static void RenderEvent(vtkObject* object, unsigned long event, void* clientdata, void* calldata);
  vtkCallbackCommand* RenderCallbackCommand;
  unsigned long ObserverTag;

  /** Is the tracked camera is enabled */
  bool Enabled;

  /** The camera should be drawn into the HMD - Option for the user that may want to activate the camera but not to respresent it */
  bool DrawingEnabled;

  /** Type of frame from the Tracked Camera : distorted/Undistorted/MaximumUndistorted*/
  vr::EVRTrackedCameraFrameType frameType;

  /** Filter to Center the image - change information of a vtkImageData*/
  //vtkSmartPointer<vtkImageChangeInformation> changeInformation;

  /** Filter to get a port on the vtkImageData to feed the Mapper*/
  vtkPassThrough *pass;

  /** The principal Scene renderer*/
  vtkSmartPointer<vtkOpenVRRenderer> Renderer;
  /** The background renderer to display tracked camera*/
  vtkSmartPointer<vtkOpenVRRenderer> backgroundRenderer;
  /** The actor representing the tracked camera*/
  vtkSmartPointer<vtkActor> TrackedCameraActor;
  /** Mapper of the TrackedCameraActor*/
  vtkSmartPointer<vtkPolyDataMapper> mapper;
  /** Definitions of the surface mapped to the TrackedcameraActor*/
  vtkSmartPointer<vtkPoints> points;
  vtkSmartPointer<vtkPolygon> polygon;
  vtkSmartPointer<vtkCellArray> polygons;
  vtkSmartPointer<vtkPolyData> polygonPolyData;
  /** Texture and his coordinates of the TrackedCamera*/
  vtkSmartPointer<vtkTexture> texture;
  vtkSmartPointer<vtkFloatArray> textureCoordinates;

private:
  vtkOpenVRTrackedCamera(const vtkOpenVRTrackedCamera&) = delete;
  void operator=(const vtkOpenVRTrackedCamera&) = delete;
};
#endif //vtkopenvrtrackedcamera_h
