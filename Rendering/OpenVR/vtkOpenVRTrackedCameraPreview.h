/*=========================================================================

Program:   Visualization Toolkit
Module:    vtkOpenVRTrackedCameraPreview.h

--Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
--All rights reserved.
--See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
* @class   vtkOpenTrackedCameraPreview
* @brief   Preview OpenVR tracked Camera in HMD
*
* vtkOpenVRTrackedCameraPreview support for VR Front camera (or trackedCamera)
*/

#ifndef vtkopenvrtrackedcamerapreview_h
#define vtkopenvrtrackedcamerapreview_h

#include "vtkRenderingOpenVRModule.h" // For export macro
#include "vtkImageData.h"

#include <openvr.h> //for ivars

class VTKRENDERINGOPENVR_EXPORT vtkOpenVRTrackedCameraPreview : public vtkObject
{
public:
  static vtkOpenVRTrackedCameraPreview *New();
  vtkTypeMacro(vtkOpenVRTrackedCameraPreview, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override { this->Superclass::PrintSelf(os, indent); }

  /**
  * Initialization of the preview, allocate dimensions to the SourceImage
  */
  void Init(uint32_t nFrameWidth, uint32_t nFrameHeight);

  /**
  * Fill the vtkImageData with the Current framebuffer of the tracked camera
  */
  void SetFrameImage(const uint8_t *pFrameImage, uint32_t nFrameWidth, uint32_t nFrameHeight, const vr::CameraVideoStreamFrameHeader_t *pFrameHeader);

  /**
  * Get the source image computed from the source buffer
  */
  vtkGetMacro(SourceImage, vtkImageData*);

protected:
  vtkOpenVRTrackedCameraPreview();
  ~vtkOpenVRTrackedCameraPreview();

  /**
  * Image source to fill and return for display
  */
  vtkImageData *SourceImage;

  /**
  * Current Frame Header from the camera
  */
  vr::CameraVideoStreamFrameHeader_t CurrentFrameHeader;

private:
  vtkOpenVRTrackedCameraPreview(const vtkOpenVRTrackedCameraPreview&) = delete;
  void operator=(const vtkOpenVRTrackedCameraPreview&) = delete;
};

#endif //vtkopenvrfrontcamerapreview_h
