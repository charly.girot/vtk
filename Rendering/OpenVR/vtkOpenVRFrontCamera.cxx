/*=========================================================================

Program:   Visualization Toolkit

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkOpenVRFrontCamera.h"

//#include "vtkCallbackCommand.h"
//#include "vtkDataArray.h"
//#include "vtkImageData.h"
//#include "vtkInteractorStyle3D.h"
#include "vtkObjectFactory.h" //vtkStandardNewMacro
//#include "vtkOpenVRCamera.h"
//#include "vtkOpenVRRenderWindow.h"
#include "vtkOpenVRRenderWindowInteractor.h" //rw->MakeCurrent()
//#include "vtkPointData.h"
//#include "vtkRenderer.h"
//#include "vtkRendererCollection.h"
#include "vtkTextureObject.h"
//#include "vtksys/SystemTools.hxx"

#include <iostream>
#include <cstdarg>
#include <stdio.h>
//#include <stdarg.h>



vtkStandardNewMacro(vtkOpenVRFrontCamera);

vtkOpenVRFrontCamera::vtkOpenVRFrontCamera()
{


	this->pHMD = nullptr;	
	this->m_pVRTrackedCamera = nullptr;
	this->m_TrackedCameraHandle = 0;	
	this->m_nCameraFrameWidth = 0;
	this->m_nCameraFrameHeight = 0; 
	this->m_nCameraFrameBufferSize = 0;
	this->m_pCameraFrameBuffer = 0;
	this->m_nLastFrameSequence = 0;
	this->TextureData = nullptr;

	vtkDebugMacro(<< "Build:%s %s " << __DATE__ << " " << __TIME__);
	std::cout << "Build : " << __DATE__ << " " << __TIME__ << std::endl; 
}


vtkOpenVRFrontCamera::~vtkOpenVRFrontCamera()
{
	this->pHMD = nullptr; //destroyed by vrkOpenVRRenderWindow
	this->m_pVRTrackedCamera = nullptr;
	delete[] this->TextureData;
	this->TextureData = 0;
}

void vtkOpenVRFrontCamera::Show()
{
	this->StartVideoPreview();
	this->Render();
}

void vtkOpenVRFrontCamera::Hide()
{
	this->StopVideoPreview();
}


bool vtkOpenVRFrontCamera::Initialize(vtkOpenVRRenderWindow *rw)
{
	vtkDebugMacro("Initializing Front Camera");
	std::cout << "Initializing Front Camera " << std::endl; 

	m_pVRTrackedCamera = vr::VRTrackedCamera();
	if (!m_pVRTrackedCamera)
	{
		vtkErrorMacro("Unable to get Tracked Camera Interface");
		std::cout << "Unable to get tracked camera interface" << std::endl; 
		return false;
	}

	bool bHasCamera = false;
	vr::EVRTrackedCameraError nCameraError = m_pVRTrackedCamera->HasCamera(vr::k_unTrackedDeviceIndex_Hmd, &bHasCamera);
	if (nCameraError != vr::VRTrackedCameraError_None || !bHasCamera)
	{
		vtkErrorMacro(<< "No Tracked Camera Available , " << m_pVRTrackedCamera->GetCameraErrorNameFromEnum(nCameraError));
		std::cout << " No tracked camera available" << std::endl; 
		return false;
	}
	this->pHMD = rw->GetHMD();
	this->Window = rw;

	//Accessing the FW description is just a further check to ensure camera communication
	// is valid as expected
	vr::ETrackedPropertyError propertyError;// = vr::TrackedProp_Success;
	char buffer[1024];

	pHMD->GetStringTrackedDeviceProperty(
		vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_CameraFirmwareDescription_String,
		buffer,	sizeof(buffer),	&propertyError);
	if (propertyError != vr::TrackedProp_Success)
	{
		vtkErrorMacro("Failed to get tracked camera firmware description");
		std::cout << "Failed to get tracked camera firmware description" << std::endl; 
		return false;
	}
	pHMD->GetStringTrackedDeviceProperty(vr::k_unTrackedDeviceIndex_Hmd,
		vr::Prop_CameraFirmwareDescription_String, buffer, sizeof(buffer), &propertyError);
	if (propertyError != vr::TrackedProp_Success)
	{
		vtkErrorMacro("Error Initializing the front camera");
		std::cout << "Error Initializing the front camera " << std::endl; 
		return false;
	}

	vtkDebugMacro(<< "Camera Firmware : " << buffer);
	std::cout << "Camera Firmware " << buffer << std::endl; 

	rw->MakeCurrent();

	this->m_TrackedCameraTexture->SetContext(rw);

	//Delete any old texture data
	if (this->TextureData)
	{
		delete[] this->TextureData;
		this->TextureData = 0;
	}

	this->StartVideoPreview();

	return true;
}

void vtkOpenVRFrontCamera::Render()
{
	std::cout << "Rendering" << std::endl; 
	//Get the frame Header
	vr::CameraVideoStreamFrameHeader_t frameHeader;
	vr::EVRTrackedCameraError nCameraError = m_pVRTrackedCamera->GetVideoStreamFrameBuffer(
		m_TrackedCameraHandle, vr::VRTrackedCameraFrameType_Undistorted,
		nullptr, 0, &frameHeader, sizeof(frameHeader));

	//skip rendering if the front camera (texture) isn't visible
	if (!vr::VRTrackedCamera() ||
		!vr::VRTrackedCamera()->GetVideoStreamTextureGL(m_TrackedCameraHandle, vr::VRTrackedCameraFrameType_Undistorted,
			nullptr, &frameHeader, sizeof(frameHeader)) )
		return;

	this->Window->MakeCurrent();

	std::cout << "Render Middle" << std::endl;

	int dims[2];
	dims[0] = frameHeader.nWidth;// this->m_TrackedCameraTexture->GetWidth();
	dims[1] = frameHeader.nHeight;// this->m_TrackedCameraTexture->GetHeight();
	std::cout << "dims m_TrackedCameraTexture " << dims[0] << " " << dims[1] << std::endl; 

	GLuint unTexture = this->m_TrackedCameraTexture->GetHandle();
	if (unTexture != 0)
	{
		vr::Texture_t texture{ (void*)(uintptr_t)unTexture, vr::TextureType_OpenGL, vr::ColorSpace_Auto };
	}
	std::cout << "Render Over" << std::endl; 
	this->OnDisplayRefreshTimeOut();
}


void vtkOpenVRFrontCamera::OnDisplayRefreshTimeOut()
{
	std::cout << "DisplayRefresh" << std::endl; 
	if (!this->m_pVRTrackedCamera || !this->m_TrackedCameraHandle)
		return;

	////Gestion timer

	//get the frame header only
	vr::CameraVideoStreamFrameHeader_t frameHeader;
	vr::EVRTrackedCameraError nCameraError = m_pVRTrackedCamera->GetVideoStreamFrameBuffer(
		this->m_TrackedCameraHandle, vr::VRTrackedCameraFrameType_Undistorted,
		nullptr, 0, &frameHeader, sizeof(frameHeader));
	if (nCameraError != vr::VRTrackedCameraError_None)
		return;

	if (frameHeader.nFrameSequence == m_nLastFrameSequence)
	{
		//frame hasn't changed yet, nothing to do 
		return;
	}

	////m_VideoSignalTime.restart();

	//frame has change, do the more expansive frame buffer copy
	nCameraError = m_pVRTrackedCamera->GetVideoStreamFrameBuffer(
		this->m_TrackedCameraHandle, vr::VRTrackedCameraFrameType_Undistorted,
		this->m_pCameraFrameBuffer, this->m_nCameraFrameBufferSize, &frameHeader, sizeof(frameHeader));
	if (nCameraError != vr::VRTrackedCameraError_None)
		return;

	this->m_nLastFrameSequence = frameHeader.nFrameSequence;

	////this->m_pCameraPreviewImage->
	SetFrameImage(m_pCameraFrameBuffer, m_nCameraFrameWidth, m_nCameraFrameHeight, &frameHeader);
}
void vtkOpenVRFrontCamera::OnToggleStreamingAction()
{
	//If triggered startPreview
	//else stop preview
}

bool vtkOpenVRFrontCamera::StartVideoPreview()
{
	//LogMessage(LogInfo, "StartVideoPreview()\n");
	vtkDebugMacro("StartVideoPreview() ");
	std::cout << "StartVideoPreview() " << std::endl;

	//Allocate for camera frame buffer requirements
	uint32_t nCameraFrameBufferSize = 0;
	if (m_pVRTrackedCamera->GetCameraFrameSize(vr::k_unTrackedDeviceIndex_Hmd,
		vr::VRTrackedCameraFrameType_Undistorted, &this->m_nCameraFrameWidth,
		&this->m_nCameraFrameHeight, &nCameraFrameBufferSize)
		!= vr::VRTrackedCameraError_None)
	{
		vtkDebugMacro("GetCameraFramebounds() Failed!");
		std::cout << "GetCameraFrameBounds() Failed " << std::endl; 
		return false;
	}

	if (nCameraFrameBufferSize && nCameraFrameBufferSize != this->m_nCameraFrameBufferSize)
	{
		delete[] this->m_pCameraFrameBuffer;
		this->m_nCameraFrameBufferSize = nCameraFrameBufferSize;
		this->m_pCameraFrameBuffer = new uint8_t[this->m_nCameraFrameBufferSize];
		memset(this->m_pCameraFrameBuffer, 0, this->m_nCameraFrameBufferSize);
	}

	this->m_nLastFrameSequence = 0;
	//m_VideoSignalStartTime.start();

	this->m_pVRTrackedCamera->AcquireVideoStreamingService(vr::k_unTrackedDeviceIndex_Hmd,
		&this->m_TrackedCameraHandle);
	if (this->m_TrackedCameraHandle == INVALID_TRACKED_CAMERA_HANDLE)
	{
		vtkDebugMacro("AcquireVideoStreamingService() Failed!");
		std::cout << "AcquiredVideoStreamingService() Failed" << std::endl; 
		return false;
	}
	return true;
}
void vtkOpenVRFrontCamera::StopVideoPreview()
{
	vtkDebugMacro("StopVideoPreview()");
	std::cout << "StopVideoPreview()" << std::endl; 
	this->m_pVRTrackedCamera->ReleaseVideoStreamingService(this->m_TrackedCameraHandle);
	this->m_TrackedCameraHandle = INVALID_TRACKED_CAMERA_HANDLE;
}

