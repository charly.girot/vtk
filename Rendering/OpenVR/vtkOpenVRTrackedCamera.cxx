/*=========================================================================

Program:   Visualization Toolkit

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/


#include "vtkCallbackCommand.h"
#include "vtkOpenVRRenderer.h"
#include "vtkOpenVRTrackedCamera.h"
#include "vtkOpenVRTrackedCameraPreview.h"
#include "vtkObjectFactory.h" //vtkStandardNewMacro
#include "vtkOpenVRRenderWindowInteractor.h" //rw->MakeCurrent()
#include "vtkPolygon.h"


vtkStandardNewMacro(vtkOpenVRTrackedCamera);

vtkOpenVRTrackedCamera::vtkOpenVRTrackedCamera()
{
  this->pHMD = nullptr;
  this->VRTrackedCamera = 0;

  this->CameraFrameWidth = 0;
  this->CameraFrameHeight = 0;
  this->CameraFrameBufferSize = 0;
  this->CameraFrameBuffer = nullptr;

  this->LastFrameSequence = 0;

  this->RenWin = nullptr;
  this->CameraPreviewImage = vtkOpenVRTrackedCameraPreview::New();
  this->SetEnabled(true);
  this->SetDrawingEnabled(false);

  this->RenderCallbackCommand = vtkCallbackCommand::New();
  this->RenderCallbackCommand->SetClientData(this);
  this->RenderCallbackCommand->SetCallback(vtkOpenVRTrackedCamera::RenderEvent);
  this->RenderCallbackCommand->SetPassiveObserver(1);

  this->pass = vtkPassThrough::New();

  this->Renderer = nullptr;

  this->frameType = vr::VRTrackedCameraFrameType_Distorted; //Default

  this->TrackedCameraActor = vtkSmartPointer<vtkActor>::New();

  this->backgroundRenderer = vtkSmartPointer<vtkOpenVRRenderer>::New();

  this->points = vtkSmartPointer<vtkPoints>::New();
  this->polygon = vtkSmartPointer<vtkPolygon>::New();
  this->polygons = vtkSmartPointer<vtkCellArray>::New();
  this->polygonPolyData = vtkSmartPointer<vtkPolyData>::New();
  this->mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  this->texture = vtkSmartPointer<vtkTexture>::New();
  this->textureCoordinates = vtkSmartPointer<vtkFloatArray>::New();

  vtkDebugMacro(<< "vtkOpenVRTrackedCamera() Build : " << __DATE__ << " " << __TIME__);
}

void vtkOpenVRTrackedCamera::SetFrameType(int type)
{
  switch (type)
  {
  case 0:
    this->frameType = vr::VRTrackedCameraFrameType_Distorted;
    break;
  case 1:
    this->frameType = vr::VRTrackedCameraFrameType_Undistorted;
    break;
  case 2:
    this->frameType = vr::VRTrackedCameraFrameType_MaximumUndistorted;
    break;
  }
}

int vtkOpenVRTrackedCamera::GetFrameType()
{
  return this->frameType;
}

std::string vtkOpenVRTrackedCamera::GetEnumFromFrameType(int type)
{
  switch ((int)type)
  {
  case 0:
    return "VRTrackedCameraFrameType_Distorted";
    break;
  case 1:
    return "VRTrackedCameraFrameType_Undistorted";
    break;
  case 2:
    return "VRTrackedCameraFrameType_MaximumUndistorted";
    break;
  default:
    return "Unknown Type";
    break;
  }
}
std::string vtkOpenVRTrackedCamera::GetEnumFromFrameType()
{
  if (this->frameType == vr::VRTrackedCameraFrameType_Distorted)
  {
    return std::string("VRTrackedCameraFrameType_Distorted");
  }
  else if (this->frameType == vr::VRTrackedCameraFrameType_Undistorted)
    return std::string("VRTrackedCameraFrameType_Undistorted");

  else if (this->frameType == vr::VRTrackedCameraFrameType_MaximumUndistorted)
    return std::string("VRTrackedCameraFrameType_MaximumUndistorted");

  else
    return "Unknown Type";
}

vtkOpenVRTrackedCamera::~vtkOpenVRTrackedCamera()
{
  this->Delete();
  this->pHMD = nullptr;
  this->VRTrackedCamera = nullptr;
}

void vtkOpenVRTrackedCamera::Initialize(vtkOpenVRRenderWindow *rw)
{
  vtkDebugMacro(<< "Initialize Tracked Camera");
  this->RenWin = rw;
  this->pHMD = rw->GetHMD();

  this->VRTrackedCamera = vr::VRTrackedCamera();
  if (!this->VRTrackedCamera)
  {
    vtkDebugMacro(<< "Unable to get tracked camera interface");
    return;
  }

  bool bHasCamera = false;
  vr::EVRTrackedCameraError nCameraError = this->VRTrackedCamera->HasCamera(vr::k_unTrackedDeviceIndex_Hmd, &bHasCamera);
  if (nCameraError != vr::VRTrackedCameraError_None || !bHasCamera)
  {
    vtkDebugMacro(<< "No Tracked Camera Available " << VRTrackedCamera->GetCameraErrorNameFromEnum(nCameraError))
      return;
  }

  // Accessing the FW description is just a further check to ensure camera communication
  // is valid as expected
  vr::ETrackedPropertyError propertyError;
  char buffer[1024];

  pHMD->GetStringTrackedDeviceProperty(
    vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_CameraFirmwareDescription_String,
    buffer, sizeof(buffer), &propertyError);
  if (propertyError != vr::TrackedProp_Success)
  {
    vtkDebugMacro(<< "failed to get tracked camera firmware description");
    return;
  }
  pHMD->GetStringTrackedDeviceProperty(vr::k_unTrackedDeviceIndex_Hmd,
    vr::Prop_CameraFirmwareDescription_String, buffer, sizeof(buffer), &propertyError);
  if (propertyError != vr::TrackedProp_Success)
  {
    vtkDebugMacro(<< "Error initializing the front camera");
    return;
  }

  vtkDebugMacro(<< "Camera Firmware " << buffer);

  rw->MakeCurrent();
}

void vtkOpenVRTrackedCamera::BuildRepresentation()
{
  this->RenWin->MakeCurrent();

  /*StartVideoPreview before Init the CameraPreviewImage to init it with Correct FrameWidth and FrameHeight*/
  this->StartVideoPreview();
  this->CameraPreviewImage->Init(this->CameraFrameWidth, this->CameraFrameHeight);

  //Read the image
  /*Get a port on the vtkImageData *CameraPreviewImage->GetSourceImage()*/
  this->pass->RemoveAllInputs();
  this->pass->AddInputData(this->CameraPreviewImage->GetSourceImage());

  vr::HmdVector2_t pFocalLength, pCenter;
  vr::EVRTrackedCameraError nCameraError = this->VRTrackedCamera->GetCameraIntrinsics(vr::k_unTrackedDeviceIndex_Hmd, vr::VRTrackedCameraFrameType_Undistorted, &pFocalLength, &pCenter);
  double fx, fy;
  fx = pFocalLength.v[0];
  fy = pFocalLength.v[1];

  // Setup  points
  double wid = (double)this->CameraFrameWidth * 1 / fx; //to multiply by 1/fx (camera calibration) so the scale can be set to one 
  double hei = (double)this->CameraFrameHeight * 1 / fy; // to multiply by 1/fy so the scale can bet set to 1 
  double a = wid / 2.0;
  double b = hei / 2.0;
  double c = -50.0;

  // Create the polygon
  int n = 10;
  this->polygon->GetPointIds()->SetNumberOfIds(2 * (n + 1)); //make a quad

  this->textureCoordinates->SetNumberOfComponents(2);
  this->textureCoordinates->SetName("TextureCoordinates");


  for (int i = 0; i <= n; i++)
  {
    double fac = (double)i;
    this->points->InsertNextPoint(-a + 2.0*fac*a / n, -b, 0.0);
    float tuple[3] = { fac / n, 0.0, 0.0 };
    this->textureCoordinates->InsertNextTuple(tuple);
  }
  for (int i = 0; i <= n; i++)
  {
    double fac = (double)i;
    this->points->InsertNextPoint(a - 2.0 * fac*a / n, b, 0.0);
    float tuple[3] = { 1.0 - fac / n, 1.0, 0.0 };
    this->textureCoordinates->InsertNextTuple(tuple);
  }

  for (int i = 0; i < 2 * (n + 1); i++)
    this->polygon->GetPointIds()->SetId(i, i);

  //Add the polygon to a list of polygons
  this->polygons->InsertNextCell(polygon);

  this->polygonPolyData->SetPoints(points); //geometry
  this->polygonPolyData->SetPolys(polygons); //topology

  this->polygonPolyData->GetPointData()->SetTCoords(textureCoordinates);

  this->mapper->SetInputData(polygonPolyData);
  this->texture->SetInputConnection(pass->GetOutputPort());

  this->TrackedCameraActor->SetMapper(mapper);
  this->TrackedCameraActor->SetTexture(texture);
  //

  if (this->DrawingEnabled)
  {
    vtkOpenVRCamera *actCamera = vtkOpenVRCamera::SafeDownCast(this->Renderer->GetActiveCamera());

    this->backgroundRenderer->SetBackground(0.2, 0.3, 0.4);
    this->RenWin->SetNumberOfLayers(2);
    this->RenWin->AddRenderer(backgroundRenderer);
    this->backgroundRenderer->SetLayer(0);
    this->backgroundRenderer->InteractiveOff();
    this->Renderer->SetLayer(1);

    this->backgroundRenderer->AddActor(TrackedCameraActor);
    this->RenWin->Render();

    // Set up the background camera to fill the renderer with the image
    double origin[3];
    double spacing[3];
    int extent[6];
    this->CameraPreviewImage->GetSourceImage()->GetOrigin(origin);
    this->CameraPreviewImage->GetSourceImage()->GetSpacing(spacing);
    this->CameraPreviewImage->GetSourceImage()->GetExtent(extent);

    /*Camera to be used on the background renderer to not shift the view*/
    vtkSmartPointer<vtkOpenVRCamera> camera = vtkSmartPointer<vtkOpenVRCamera>::New();
    camera->ParallelProjectionOn();
    double xc = origin[0] + 0.5*(extent[0] + extent[1])*spacing[0];
    double yc = origin[1] + 0.5*(extent[2] + extent[3])*spacing[1];
    double yd = (extent[3] - extent[2] + 1)*spacing[1];
    double d = actCamera->GetDistance();
    camera->SetParallelScale(0.5*yd);
    camera->SetFocalPoint(xc, yc, 0.0);
    camera->SetPosition(xc, yc, d);

    this->backgroundRenderer->SetActiveCamera(camera);
    // Render again to set the correct view
    this->RenWin->Render();
  }
}
void vtkOpenVRTrackedCamera::RenderEvent(vtkObject* object,
  unsigned long event,
  void* clientdata,
  void* calldata)
{
  vtkOpenVRTrackedCamera *self = static_cast<vtkOpenVRTrackedCamera *>(clientdata);

  vtkEventData *ed = static_cast<vtkEventData *>(calldata);

  if (self->Enabled)
  {
    self->DisplayRefreshTimeout();
    self->UpdateDisplayPosition();
    self->TrackedCameraActor->SetVisibility(self->DrawingEnabled);
  }
}

void vtkOpenVRTrackedCamera::UpdateDisplayPosition()
{
  if (!this->Enabled)
    return;

  vtkOpenVRRenderWindow* VRrenWin = vtkOpenVRRenderWindow::SafeDownCast(this->Renderer->GetRenderWindow());
  vtkOpenVRRenderWindowInteractor* VRinteractor = vtkOpenVRRenderWindowInteractor::SafeDownCast(this->Renderer->GetRenderWindow()->GetInteractor());

  if (!VRrenWin || !VRinteractor)
  {
    return;
  }
  if (this->Renderer && VRrenWin && VRinteractor)
  {
    //Update physical scale
    double physicalScale = VRinteractor->GetPhysicalScale();

    int hmdIdx = static_cast<int>(vtkEventDataDevice::HeadMountedDisplay);
    const vr::TrackedDevicePose_t &tdPose = VRrenWin->GetTrackedDevicePose(hmdIdx);

    double pos[3];
    double ppos[3];
    double wxyz[4];
    double wdir[3];
    VRinteractor->ConvertPoseToWorldCoordinates(tdPose, pos, wxyz, ppos, wdir);

    vtkOpenVRCamera *actCamera = vtkOpenVRCamera::SafeDownCast(this->Renderer->GetActiveCamera());
    if (!actCamera)
    {
      return;
    }

    double *ori = actCamera->GetOrientationWXYZ();
    vtkSmartPointer<vtkTransform> tr = vtkSmartPointer<vtkTransform>::New();
    tr->Identity();
    tr->RotateWXYZ(-ori[0], ori[1], ori[2], ori[3]);

    double *frameForward = actCamera->GetDirectionOfProjection();
    double *hmdUpWc = tr->TransformDoubleVector(0.0, 1.0, 0.0);
    double frameRight[3];
    vtkMath::Cross(hmdUpWc, frameForward, frameRight);
    vtkMath::Normalize(frameRight);

    double theta = 10.0;
    double thetaRad = theta * vtkMath::Pi() / 180.0;
    double A = std::cos(thetaRad);
    double B = std::sin(thetaRad);

    hmdUpWc[0] = hmdUpWc[0] * A + frameForward[0] * B;
    hmdUpWc[1] = hmdUpWc[1] * A + frameForward[1] * B;
    hmdUpWc[2] = hmdUpWc[2] * A + frameForward[2] * B;

    frameForward[0] = -hmdUpWc[0] * B + frameForward[0] * A;
    frameForward[1] = -hmdUpWc[1] * B + frameForward[1] * A;
    frameForward[2] = -hmdUpWc[2] * B + frameForward[2] * A;

    vtkMath::Normalize(frameForward);
    vtkMath::Normalize(hmdUpWc);

    double offsetScreen[3] = { 1.2 ,0.025 , -0.035 };

    double frameOrigin[3] =
    {
      pos[0] ,
      pos[1] ,
      pos[2]
    };

    double framePosition[3] = {
      frameOrigin[0],
      frameOrigin[1],
      frameOrigin[2]
    };

    //apply offset along the frameForward axis 
    framePosition[0] += offsetScreen[0] * frameForward[0];
    framePosition[1] += offsetScreen[0] * frameForward[1];
    framePosition[2] += offsetScreen[0] * frameForward[2];

    //apply offset along the frameRight axis
    framePosition[0] += offsetScreen[1] * frameRight[0];
    framePosition[1] += offsetScreen[1] * frameRight[1];
    framePosition[2] += offsetScreen[1] * frameRight[2];

    //apply offset along the frameUp axis
    framePosition[0] += offsetScreen[2] * hmdUpWc[0];
    framePosition[1] += offsetScreen[2] * hmdUpWc[1];
    framePosition[2] += offsetScreen[2] * hmdUpWc[2];

    this->TrackedCameraActor->SetPosition(framePosition);
    this->TrackedCameraActor->SetOrientation(tr->GetOrientation());


    double s[3] = { 1.0 / (10 * physicalScale*(double)VRrenWin->GetSize()[0] / (double)this->CameraFrameWidth),
      1.0 / (10 * physicalScale*(double)VRrenWin->GetSize()[1] / (double)this->CameraFrameHeight),
      1.0 };
    this->TrackedCameraActor->SetScale(1);
  }
}


void vtkOpenVRTrackedCamera::StartVideoPreview()
{
  vtkDebugMacro(<< "StartVideoPreview()");
  // Allocate for camera frame buffer requirements
  uint32_t nCameraFrameBufferSize = 0;

  if (this->VRTrackedCamera->GetCameraFrameSize(vr::k_unTrackedDeviceIndex_Hmd, frameType, &this->CameraFrameWidth, &this->CameraFrameHeight, &nCameraFrameBufferSize) != vr::VRTrackedCameraError_None)
  {
    vtkDebugMacro(<< "GetCameraFrameBounds() Failed");
    return;
  }

  if (nCameraFrameBufferSize && nCameraFrameBufferSize != this->CameraFrameBufferSize)
  {
    delete[] this->CameraFrameBuffer;
    this->CameraFrameBufferSize = nCameraFrameBufferSize;
    this->CameraFrameBuffer = new uint8_t[this->CameraFrameBufferSize];
    memset(this->CameraFrameBuffer, 0, this->CameraFrameBufferSize);
  }

  this->LastFrameSequence = 0;

  this->VRTrackedCamera->AcquireVideoStreamingService(vr::k_unTrackedDeviceIndex_Hmd, &this->VRTrackedCameraHandle);
  if (this->VRTrackedCameraHandle == INVALID_TRACKED_CAMERA_HANDLE)
  {
    vtkDebugMacro(<< "AcquireVideoStreamingService() Failed");
    return;
  }
  this->TrackedCameraActor->VisibilityOn();
}

void vtkOpenVRTrackedCamera::StopVideoPreview()
{
  vtkDebugMacro(<< "Stop Video Preview");

  if (!this->VRTrackedCamera)
  {
    vtkDebugMacro(<< "Tracked Camera nullptr");
  }

  if (this->TrackedCameraActor->GetVisibility())
  {
    this->TrackedCameraActor->VisibilityOff();
  }
}

void vtkOpenVRTrackedCamera::DisplayRefreshTimeout()
{
  if (!this->Enabled)
  {
    return;
  }

  if (!this->VRTrackedCamera || !this->VRTrackedCameraHandle)
  {
    return;
  }

  //get the frame header only
  vr::CameraVideoStreamFrameHeader_t frameHeader;
  vr::EVRTrackedCameraError nCameraError = this->VRTrackedCamera->GetVideoStreamFrameBuffer(this->VRTrackedCameraHandle, frameType, nullptr, 0, &frameHeader, sizeof(frameHeader));
  if (nCameraError != vr::VRTrackedCameraError_None)
  {
    return;
  }

  if (frameHeader.nFrameSequence == this->LastFrameSequence)
  {
    // frame hasn't changed yet, nothing to do
    return;
  }

  // Frame has changed, do the more expensive frame buffer copy
  nCameraError = this->VRTrackedCamera->GetVideoStreamFrameBuffer(this->VRTrackedCameraHandle, frameType, this->CameraFrameBuffer, this->CameraFrameBufferSize, &frameHeader, sizeof(frameHeader));
  if (nCameraError != vr::VRTrackedCameraError_None)
  {
    return;
  }
  //TO DO : There should be a less costing way to get the image using the frame buffer without having to copy it in a vtkImageData to represent it.
  //this->VRTrackedCamera->GetVideoStreamTextureGL()
  this->LastFrameSequence = frameHeader.nFrameSequence;
  this->CameraPreviewImage->SetFrameImage(this->CameraFrameBuffer, this->CameraFrameWidth, this->CameraFrameHeight, &frameHeader);

  this->pass->Modified();
  this->texture->Modified();
}

void vtkOpenVRTrackedCamera::SetEnabled(bool val)
{
  if (val == this->Enabled)
  {
    return;
  }

  this->Enabled = val;
  if (!this)
  {
    return;
  }
  if (!this->Enabled)
  {
    this->StopVideoPreview();
  }
  this->Modified();
}

void vtkOpenVRTrackedCamera::SetDrawingEnabled(bool enable)
{
  if (enable == this->DrawingEnabled)
  {
    return;
  }
  this->DrawingEnabled = enable;
  if (!this)
  {
    return;
  }

  this->Modified();
}
void vtkOpenVRTrackedCamera::SetRenderer(vtkRenderer *ren)
{
  vtkOpenVRRenderer *vrRen = vtkOpenVRRenderer::SafeDownCast(ren);
  if (!vrRen)
  {
    std::cerr << "Not a VR Renderer " << std::endl;
    return;
  }
  if (vrRen == this->Renderer)
  {
    return;
  }

  if (this->Renderer)
  {
    vtkOpenVRRenderWindowInteractor *interactor = vtkOpenVRRenderWindowInteractor::SafeDownCast(this->Renderer->GetRenderWindow()->GetInteractor());
    if (!interactor)
    {
      return;
    }
    interactor->RemoveObserver(this->ObserverTag);
  }

  this->Renderer = vrRen;
  if (this->Renderer)
  {
    vtkOpenVRRenderWindowInteractor *interactor = vtkOpenVRRenderWindowInteractor::SafeDownCast(this->Renderer->GetRenderWindow()->GetInteractor());
    if (!interactor)
    {
      return;
    }
    this->ObserverTag = interactor->AddObserver(vtkCommand::Move3DEvent, this->RenderCallbackCommand, 10.0);
  }

  this->Modified();
}

//----------------------------------------------------------------------------
vtkRenderer* vtkOpenVRTrackedCamera::GetRenderer()
{
  return this->Renderer;
}
