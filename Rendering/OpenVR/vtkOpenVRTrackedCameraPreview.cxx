/*=========================================================================

Program:   Visualization Toolkit

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkOpenVRTrackedCameraPreview.h"
#include "vtkObjectFactory.h" //vtkStandardNewMacro

vtkStandardNewMacro(vtkOpenVRTrackedCameraPreview);
vtkOpenVRTrackedCameraPreview::vtkOpenVRTrackedCameraPreview()
{
  this->SourceImage = vtkImageData::New();
  memset(&this->CurrentFrameHeader, 0, sizeof(this->CurrentFrameHeader));
}

vtkOpenVRTrackedCameraPreview::~vtkOpenVRTrackedCameraPreview()
{
}

void vtkOpenVRTrackedCameraPreview::Init(uint32_t nFrameWidth, uint32_t nFrameHeight)
{
  this->SourceImage->SetDimensions((int)nFrameWidth, (int)nFrameHeight, 1);
  this->SourceImage->SetSpacing(1.0, 1.0, 1.0);
  this->SourceImage->SetOrigin(0.0, 0.0, 0.0);
  this->SourceImage->AllocateScalars(VTK_UNSIGNED_CHAR, 3);
}

void vtkOpenVRTrackedCameraPreview::SetFrameImage(const uint8_t *pFrameImage, uint32_t nFrameWidth, uint32_t nFrameHeight, const vr::CameraVideoStreamFrameHeader_t *pFrameHeader)
{
  if (pFrameHeader)
  {
    this->CurrentFrameHeader = *pFrameHeader;
  }

  if (pFrameImage && nFrameWidth && nFrameHeight)
  {
    if (this->SourceImage &&
      ((uint32_t)this->SourceImage->GetDimensions()[0] != nFrameWidth ||
      (uint32_t)this->SourceImage->GetDimensions()[1] != nFrameHeight))
    {
      //dimension changed
      this->SourceImage->Delete();
      this->SourceImage = nullptr;
    }

    if (!this->SourceImage)
    {
      // allocate to expected dimensions
      this->SourceImage = vtkImageData::New();
      this->SourceImage->SetDimensions((int)nFrameWidth, (int)nFrameHeight, 1);
      this->SourceImage->AllocateScalars(VTK_UNSIGNED_CHAR, 3);
    }
    for (int y = (int)nFrameHeight - 1; y >= 0; y--)
    {
      for (int x = 0; x < (int)nFrameWidth; x++)
      {
        int* dims = SourceImage->GetDimensions();
        unsigned char* pixel = static_cast<unsigned char*>(this->SourceImage->GetScalarPointer(x, y, 0));

        if (!pixel)
        {
          vtkDebugMacro(<< "Pixel Null, check for errors");
          return;
        }
        pixel[0] = pFrameImage[0];
        pixel[1] = pFrameImage[1];
        pixel[2] = pFrameImage[2];
        pFrameImage += 4;
      }
    }
  }
}
