/*=========================================================================

Program:   Visualization Toolkit
Module:    vtkOpenVRFrontCamera.h

--Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
--All rights reserved.
--See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
* @class   vtkOpenVRFrontCamera
* @brief   OpenVR front camera (or tracked camera)
*
* vtkOpenVRFrontCamera support for VR Front camera (or trackedCamera)
*/
#ifndef vtkopenvrfrontcamera_h
#define vtkopenvrfrontcamera_h

#include "vtkRenderingOpenVRModule.h" // For export macro
#include "vtkObject.h"
#include <openvr.h> //for ivars
#include <vector> // ivars
#include "vtkNew.h" // for ivars
#include "vtkWeakPointer.h" // for ivars


class vtkOpenVRRenderWindow;
class vtkTextureObject;



class VTKRENDERINGOPENVR_EXPORT vtkOpenVRFrontCamera : public vtkObject
{
public:
	static vtkOpenVRFrontCamera *New();
	vtkTypeMacro(vtkOpenVRFrontCamera, vtkObject);

	/** Render the front Camera */
	virtual void Render();

	/** Initialize the front camera */
	virtual bool Initialize(vtkOpenVRRenderWindow *rw);

	/** Get handle to the front camera */
	vr::TrackedCameraHandle_t GetFrontCameraHandle() { return this->m_TrackedCameraHandle; }

	/** Get handle to the front camera texture */
	vtkTextureObject *GetFrontCameraTexture() { return this->m_TrackedCameraTexture; }

	//@{
	/**
	* methods to support events on the rendered front camera ... NOT USEFUL FOR US ? 
	*/
	void OnDisplayRefreshTimeOut();
	void OnToggleStreamingAction();

	//@}

	void Show();
	void Hide();

	bool StartVideoPreview();
	void StopVideoPreview();

protected:
	vtkOpenVRFrontCamera();
	~vtkOpenVRFrontCamera();

	vr::IVRSystem *pHMD;

	//for the front camera
	vr::IVRTrackedCamera		*m_pVRTrackedCamera;
	vr::TrackedCameraHandle_t	m_TrackedCameraHandle;
	vtkNew<vtkTextureObject> m_TrackedCameraTexture;

	uint32_t				m_nCameraFrameWidth;
	uint32_t				m_nCameraFrameHeight;
	uint32_t				m_nCameraFrameBufferSize;
	uint8_t					*m_pCameraFrameBuffer;

	uint32_t				m_nLastFrameSequence;

	//vtkNew<vtkImageData>  	m_pCameraPreviewImage;

	unsigned char *TextureData;

	vtkWeakPointer<vtkOpenVRRenderWindow> Window;


	//Second class
	//vtkImageData		*m_pSourceImage;
	//vr::CameraVideoStreamFrameHeader_t m_CurrentFrameHeader;
private :
	vtkOpenVRFrontCamera(const vtkOpenVRFrontCamera&) = delete;
	void operator=(const vtkOpenVRFrontCamera&) = delete;
	
};

#endif 